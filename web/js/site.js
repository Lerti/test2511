$(document).ready(function() {
    function changeUserData(name, value, parent) {
        if (value !== '') {
            parent.find('.user-' + name).text(value);
        }
    }

    $('.show-update-div').on('click', (e) => {
        let id = $(e.currentTarget).data('id');
        $('.update-user[data-id="' + id + '"]').show();
    });

    $('.save-user-info').on('click', (e) => {
        let item = $(e.currentTarget).parents(".update-user");
        let id = item.data('id');
        let login = item.find('.new-login').val();
        let password = item.find('.new-password').val();
        let role = item.find('.new-role').val();

        if (login === "" && password === "" && role === "") {
            alert('Введите данные!');
        } else {
            $.ajax({
                url: '/index.php?r=site%2Fupdate-user',
                type: "POST",
                data: {
                    id: id,
                    login: login,
                    password: password,
                    role: role,
                },
                success: function (data) {
                    if (!data) {
                        alert ('Обновление данных не удалось. Попробуйте еще раз!')
                    } else {
                        $('.update-user[data-id="' + id + '"]').hide();
                        let info = $('.show-user[data-id="' + id + '"]');
                        changeUserData('login', login, info);
                        changeUserData('password', password, info);
                        changeUserData('role', role, info);
                    }
                },
            });
        }
    });

    $('.update-company-info').on('click', (e) => {
        let name = $('#companiesinfo-name').val();
        let inn = $('#companiesinfo-inn').val();
        let general_manager = $('#companiesinfo-general_manager').val();
        let address = $('#companiesinfo-address').val();

        if (name === "" && inn === "" && general_manager === "" && address === "") {
            alert('Введите данные!');
        } else {
            $.ajax({
                url: $('.update-user-form').attr('action'),
                type: "POST",
                data: {
                    id: $(e.currentTarget).data('id'),
                    name: name,
                    inn: inn,
                    general_manager: general_manager,
                    address: address
                },
                success: function (data) {
                    if (!data) {
                        alert ('Обновление данных не удалось. Попробуйте еще раз!')
                    } else {
                        alert('Данные об этой компании обновлены!')
                    }
                },
            });
        }
    });
});
