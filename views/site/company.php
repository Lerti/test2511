<?php

use app\models\CompaniesInfo;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/**
 * @var int $role
 * @var CompaniesInfo $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;

?>
<div>
    <?php if ($role === User::ROLE_ADMIN) : ?>
        <?php $form = ActiveForm::begin([
            'options' => ['class' => 'update-user-form']
        ]); ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'inn') ?>
        <?= $form->field($model, 'general_manager') ?>
        <?= $form->field($model, 'address') ?>

        <div class="form-group">
            <?= Html::button('Сохранить', ['class' => 'btn btn-success update-company-info', 'data-id' => $model->id]) ?>
        </div>
        <?php ActiveForm::end(); ?>
    <?php else : ?>
        <?php foreach ($model->attributes as $name => $value) { ?>
            <div class="themed-grid-col"><?= $name . ' = ' . $value ?></div>
        <?php } ?>
    <?php endif; ?>
</div>
