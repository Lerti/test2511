<?php

use app\models\User;

/* @var $users User[] */

$this->title = 'Список пользователей';
$this->params['breadcrumbs'][] = $this->title;

$menu = [
    ['name' => 'Имя', 'class' => 'login'],
    ['name' => 'Пароль', 'class' => 'password'],
    ['name' => 'Роль', 'class' => 'role'],
    ['name' => '', 'class' => 'change']
];
?>

<h1 class="title"><?= $this->title ?></h1>
<div>
    <div class="row">
        <?php foreach ($menu as $item) : ?>
            <div class="col-3 themed-grid-col"><?= $item['name'] ?></div>
        <?php endforeach; ?>
    </div>
    <?php foreach ($users as $user): ?>
        <div class="row show-user" data-id="<?= $user->id ?>">
            <?php foreach ($menu as $item) { ?>
                <?php if($item['class'] !== 'change') { ?>
                    <div class="col-3 themed-grid-col user-<?= $item['class'] ?>">
                        <?= $item['class'] !== 'role' ? $user->$item['class'] : $user->role->name ?>
                    </div>
                <?php } else { ?>
                    <div class="col-3 themed-grid-col">
                        <button class="btn btn-primary show-update-div" data-id="<?= $user->id ?>">Изменить</button>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="row update-user" data-id="<?= $user->id ?>">
            <?php foreach ($menu as $item) { ?>
                <?php if($item['class'] !== 'change') { ?>
                    <div class="col-3 themed-grid-col">
                        <input type="text" class="form-control new-<?= $item['class'] ?>" placeholder="<?= $item['name'] ?>">
                    </div>
                <?php } else { ?>
                    <div class="col-3 themed-grid-col">
                        <button class="btn btn-success save-user-info">Сохранить</button>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    <?php endforeach; ?>
</div>
