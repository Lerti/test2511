<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/**
 * @var $dataProvider ActiveDataProvider
 * @var $isAdmin bool
 */

$this->title = 'Спискок компаний';
?>
<div class="site-index">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'name',
                'inn',
                'general_manager',
                'address',
                [
                    'label' => 'Просмотр',
                    'value' => function ($data) use ($isAdmin) {
                        $text = $isAdmin ? 'Изменить' : 'Просмотреть';
                        return Html::a($text, ['site/update-company','id' => $data->id],
                            ['class' => 'btn btn-success']);
                    },
                    'format' => 'raw',
                ],
            ]
        ]
    ); ?>
</div>
