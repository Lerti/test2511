<?php namespace app\controllers;

use app\models\CompaniesInfo;
use app\models\Roles;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            $dataProvider = new ActiveDataProvider([
                'query' => CompaniesInfo::find()
            ]);
            $isAdmin = Yii::$app->user->identity->isAdmin();
            return $this->render('index', ['dataProvider' => $dataProvider, 'isAdmin' => $isAdmin]);
        } else {
            return $this->redirect(Url::to(['site/login']));
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionUsers()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
            $users = User::find()->with('role')->all();
            return $this->render('users', ['users' => $users]);
        } else {
            return $this->redirect(Url::to(['site/login']));
        }
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionUpdateUser()
    {
        $post = Yii::$app->request->post();
        $answer = false;
        if (Yii::$app->request->isAjax && isset($post['id'])) {
            $user = User::findOne($post['id']);
            if ($user) {
                $newLogin = trim($post['login']);
                $newPassword = trim($post['password']);
                $newRole = trim($post['role']);
                if ($newLogin && $user->login !== $newLogin) {
                    $user->login = $newLogin;
                }
                if ($newPassword && $user->password !== $newPassword) {
                    $user->password = $newPassword;
                }
                if ($newRole) {
                    $role = Roles::findOne(['name' => $newRole]);
                    if (!$role) {
                        return $answer;
                    } else {
                        if ($user->role_id !== $role->id) {
                            $user->role_id = $role->id;
                        }
                    }
                }
                $answer = $user->save();
            }
        } else {
            throw new NotFoundHttpException();
        }
        return $answer;
    }

    /**
     * @param int $id
     * @return bool|string
     * @throws NotFoundHttpException
     */
    public function actionUpdateCompany($id)
    {
        $post = Yii::$app->request->post();
        if (Yii::$app->request->isAjax && isset($post['id'])) {
            return self::updateCompanyInfo($post);
        } else if (!Yii::$app->user->isGuest) {
            $role = Yii::$app->user->identity->role->id;
            $model = CompaniesInfo::find()->where(['id' => $id])->one();
            if (!$model) {
                throw new NotFoundHttpException();
            }
            return $this->render('company', ['model' => $model, 'role' => $role]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @param array $info
     * @return bool
    */
    public static function updateCompanyInfo($info)
    {
        $company = CompaniesInfo::findOne($info['id']);
        unset($info['id']);
        $answer = false;
        if ($company) {
            foreach ($info as $name => $value) {
                if ($value !== $company->$name) {
                    $company->$name = $value;
                }
            }
            $answer = $company->save();
        }
        return $answer;
    }
}
