<?php namespace app\models;

use Yii;

/**
 * This is the model class for table "companies_info".
 *
 * @property int $id
 * @property string $name
 * @property int $inn
 * @property string $general_manager
 * @property string $address
 */
class CompaniesInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'companies_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'inn', 'general_manager', 'address'], 'required'],
            [['name', 'general_manager', 'address'], 'string'],
            [['inn'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'inn' => 'ИНН',
            'general_manager' => 'Директор',
            'address' => 'Адрес',
        ];
    }
}
