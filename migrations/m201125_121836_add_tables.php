<?php

use yii\db\Migration;

/**
 * Class m201125_121836_add_tables
 */
class m201125_121836_add_tables extends Migration
{
    const COMPANIES_TABLE = 'companies_info';
    const COMPANIES_INFO = [
        ['Первая компания', '111111111111', 'Петр Алексеевич Первый', 'СПб, ул. Петровская 1'],
        ['Вторая компания', '222222222222', 'Петр Алексеевич Второй', 'СПб, ул. Петровская 2'],
        ['Третья компания', '333333333333', 'Петр Федорович Третий', 'СПб, ул. Петровская 3'],
    ];

    const ROLES_TABLE = 'roles';
    const ROLES_INFO = [
        ['Администратор'],
        ['Гость'],
    ];

    const USERS_TABLE = 'users';
    const USERS_INFO = [
        ['admin', 'admin', 1],
        ['guest', 'guest', 2],
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::COMPANIES_TABLE, [
            'id' => $this->primaryKey(),
            'name' =>  $this->text()->notNull(),
            'inn' =>  $this->bigInteger(12)->notNull(),
            'general_manager' =>  $this->text()->notNull(),
            'address' => $this->text()->notNull(),
        ]);
        $this->batchInsert(self::COMPANIES_TABLE, ['name', 'inn', 'general_manager', 'address'],
            self::COMPANIES_INFO);

        $this->createTable(self::ROLES_TABLE, [
            'id' => $this->primaryKey(),
            'name' =>  $this->text()->notNull(),
        ]);
        $this->batchInsert(self::ROLES_TABLE, ['name'],
            self::ROLES_INFO);

        $this->createTable(self::USERS_TABLE, [
            'id' => $this->primaryKey(),
            'login' => $this->text()->notNull(),
            'password' => $this->text()->notNull(),
            'role_id' =>  $this->integer()->defaultValue(2),
        ]);
        $this->addForeignKey(
            'fk_user_role',
            self::USERS_TABLE,
            ['role_id'],
            self::ROLES_TABLE,
            'id'
        );
        $this->batchInsert(self::USERS_TABLE, ['login', 'password', 'role_id'],
            self::USERS_INFO);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::USERS_TABLE);
        $this->dropTable(self::ROLES_TABLE);
        $this->dropTable(self::COMPANIES_TABLE);
    }
}
